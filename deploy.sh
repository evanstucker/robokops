#!/bin/bash

function usage {
cat <<EOF
$(basename $0) deploys releases of apps to a Kubernetes cluster.
 
Usage:
 
    $(basename $0) [-h] -r release -a app
 
    -h  Help
    -r  Release
    -a  Application
  
Examples:
  
    Deploy the "take-two" release of the "cassandra" app:
    $(basename $0) -r take-two -a cassandra
 
EOF
exit 1
}

while getopts :a:hr: option; do
    case $option in
        a) app=$OPTARG;;
        r) release=$OPTARG;;
        h) usage;;
        \?) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

if [[ $# -eq 0 ]]; then 
  usage
elif [[ "$release" == "" ]]; then
  echo "ERROR: Release name is required." >&2
  exit 1
elif [[ "$app" == "" ]]; then
  echo "ERROR: Application name is required." >&2
  exit 1
fi

source custom_env.sh
source static_env.sh

mkdir -p "manifests/${release}"
helm fetch --untar --untardir charts "stable/$app"
helm template --values <(cat values/${release}-keycloak.values.yaml | envsubst) --output-dir "manifests/${release}" --name $release charts/keycloak
kubectl apply --recursive --filename "manifests/${release}/keycloak"
