# https://github.com/kubernetes/kops/blob/master/docs/aws.md#scenario-3-subdomain-for-clusters-in-route53-leaving-the-domain-at-another-registrar
variable "cluster_domain" {}
resource "aws_route53_zone" "kops_zone" {
  name = "${var.cluster_domain}"
}
