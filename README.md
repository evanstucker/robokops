# n00bernetes

## Overview

If you don't feel like a n00b when working with Kubernetes, then you're probably not paying attention. Kubernetes and the tools that support it are constantly evolving. This is an attempt to completely document how to build a Kubernetes cluster with these features:

* affordable (strongly prefer free software)
* compliant (SOC 2, PCI, CIS, etc.)
* documented
* efficient
* highly available
* maintainable (prefer open source software)
* scalable (prefer software that scales in a distributed, horizontal fashion)
* secure by default (no anonymous access, no default passwords, etc.)
* stable
* supportable (prefer popular, CNCF community supported software)
* gitops

In other words, a "production-ready", "enterprise grade" Kubernetes cluster.

### Why Kubernetes?  

Because it's winning the popularity contest compared to Mesos, Docker Swarm, and Nomad.

### Why AWS?  

This example uses AWS for now, but I hope to eventually do a comparison between Amazon and Google and provide objective reasons for choosing one or the other.

### Gitops

I used this as a starting point for Tiller-less Helm templating:
https://blog.giantswarm.io/what-you-yaml-is-what-you-get/

### Errors, Warnings, Pitfalls, and other things to be aware of

We can't use c5 or m5 AWS instances yet, because Kops doesn't have support for NVMe. See:

* https://github.com/kubernetes/kops/issues/5694
* https://github.com/kubernetes/kops/blob/master/channels/stable#L22.

## Prerequisites

* an AWS account configured with a [named profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)
* a Gitlab (or Github) account
* [awscli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [kops](https://github.com/kubernetes/kops/blob/master/docs/install.md)
* [helm](https://docs.helm.sh/using_helm/#installing-helm)
* [jq](https://stedolan.github.io/jq/download/)

## Process

1. Create a repo in [Gitlab](https://docs.gitlab.com/ce/gitlab-basics/create-project.html) or [Github](https://help.github.com/articles/create-a-repo/) with the same name as your cluster, for example, "prod.n00bernetes.com".
1. Clone your new repo. I typically create a directory structure like "$HOME/gitlab.com/evanstucker/n00bernetes", so that's what you'll see in this documentation.
1. Clone the [n00bernetes](https://gitlab.com/evanstucker/n00bernetes) repo.
1. Set these two environment variables to appropriate directories. Note: the trailing slashes on the two dirs are important:
    ```
    export n00b_dir="${HOME}/gitlab.com/evanstucker/n00bernetes/"
    export your_dir="${HOME}/gitlab.com/your_organization/prod.yourcompany.com/"
    ```
1. Then, copy the n00bernetes files to your new repo with this command:
    ```
    rsync \ 
      --checksum \
      --exclude-from "${n00b_dir}/rsync_exclude.txt" \
      --perms \
      --recursive \
      --stats \
      --verbose \
      "$n00b_dir" "$your_dir"
    ```
1. Change to your new repo's directory.
1. Edit `custom_env.sh`.
1. Source the files, one at a time, in this order:
    ```
    source custom_env.sh
    source static_env.sh
    ```
1. If one doesn't already exist, create a bucket for your Terraform [backend](https://www.terraform.io/docs/backends/types/s3.html):
    ```
    aws s3api create-bucket --bucket "$TF_VAR_terraform_bucket"
    aws s3api put-bucket-versioning \
      --bucket "$TF_VAR_terraform_bucket" \
      --versioning-configuration Status=Enabled
    ```
1. Create backend.tf, initialize Terraform, and apply it:
    ```
    cat backend.tf.template | envsubst > backend.tf
    terraform init
    terraform apply
    ```
1. Run this to get the nameservers for your new zone:
    ```
    export zone_id=$(aws route53 list-hosted-zones | jq -r ".HostedZones[] | select(.Name == \"$NAME.\") | .Id")
    aws route53 get-hosted-zone --id "$zone_id" | jq -r '.DelegationSet.NameServers[]'
    ```
1. After you do this, create multiple NS records for the subdomain (in this example prod.n00bernetes.com) - one for each NS record that's output by the command above.
1. Do not proceed until you see all four nameservers from your local machine:
    ```
    dig +short $NAME NS
    ```
1. If you see all the nameservers, then create the cluster. This command is based on [this](https://github.com/kubernetes/kops/blob/master/docs/high_availability.md) info.
    ```
    kops create cluster \
      --bastion \
      --cloud aws \
      --encrypt-etcd-storage \
      --master-zones "${ZONES}" \
      --name "${NAME}" \
      --networking weave \
      --node-count 3 \
      --out=. \
      --target=terraform \
      --topology private \
      --yes \
      --zones "${ZONES}"
    ```
1. Workaround for bug [4049](https://github.com/kubernetes/kops/issues/4049). Run this:
    ```
    kops edit cluster
    ```
    Then add kubelet and masterKubelet under spec like this this:
    ```
    spec:
      kubelet:
        kubeletCgroups: "/systemd/system.slice"
        runtimeCgroups: "/systemd/system.slice"
      masterKubelet:
        kubeletCgroups: "/systemd/system.slice"
        runtimeCgroups: "/systemd/system.slice"
    ```
    Don't close your editor yet, there's more!
1. Change the kubeDNS provider to [CoreDNS](https://coredns.io/2018/05/21/migration-from-kube-dns-to-coredns/). Run this:
    ```
    kops edit cluster
    ```
    Then add this:
    ```
    spec:
      kubeDNS:
        provider: CoreDNS
    ```
1. Add some policies for [ExternalDNS](https://github.com/kubernetes-incubator/external-dns/blob/master/docs/tutorials/aws.md):
    ```
    kops edit cluster
    ```
    Then add this:
    ```
    spec:
      additionalPolicies:
        node: |
          [     
            {
              "Effect": "Allow",
              "Action": "route53:ChangeResourceRecordSets",
              "Resource": "arn:aws:route53:::hostedzone/*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "route53:ListHostedZones",
                "route53:ListResourceRecordSets"
              ],
              "Resource": "*"
            }
          ]
    ```
    TODO: Figure out how to use CoreDNS instead of Route53.
1. Update KOPS after edit the cluster, then use Terraform to apply it:
    ```
    kops update cluster --out=. --target=terraform --yes
    terraform apply
    ```
1. The --encrypt-etcd-storage option above has created a KMS key. You just need to update the existing gp2 StorageClass to use it:
    ```
    export kms_key_id=$(aws kms list-aliases | jq -r '.Aliases[] | select(.AliasName == "alias/aws/ebs") | .TargetKeyId')
    for sc in default gp2; do
      export sc_file="${sc}.sc.json"
      kubectl get sc $sc --export -o json | jq ".parameters += {encrypted:\"true\",kmsKeyId:\"${kms_key_id}\"}" > $sc_file
      kubectl delete sc $sc
      kubectl apply -f $sc_file --record
    done
    ```
1. Install the Helm client. We don't want to use Tiller, because that's not gitops. Instead, we're going to use Helm to generate Kubernetes Object YAML with the `helm template` commands below.
    ```
    helm init --client-only
    helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
    ```
    Alternately, to use Helm as intended:
    ```
    kubectl apply -f tiller.yaml --record
    helm init --service-account tiller --override 'spec.template.spec.containers[0].command'='{/tiller,--storage=secret}'
    helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
    helm repo update
    ```
1. Install ExternalDNS to create DNS records automatically for new services and ingresses: 
    ```
    cat external-dns.yaml.template | envsubst > manifests/external-dns.yaml
    kubectl apply -f manifests/external-dns.yaml --record
    ```
1. Install cert-manager to manage HTTPS certs. First, set some variable names: 
    ```
    export release=cai; export app=cert-manager; export branch=stable
    ```
    Then follow the rest of the "Deploy" process below. And finally, add a ClusterIssuer:
    ```
    cat cert-manager.yaml.template | envsubst > "manifests/${release}/cert-manager.yaml"
    kubectl apply -f "manifests/${release}/cert-manager.yaml" --record
    ```
1. Set up an [Ingress Controller](https://kubernetes.github.io/ingress-nginx/):
    ```
    mkdir manifests/ingress-nginx
    cd manifests/ingress-nginx
    curl -O https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
    curl -O https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/aws/service-l4.yaml
    curl -O https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/aws/patch-configmap-l4.yaml
    cd -
    kubectl apply -R -f manifests/ingress-nginx --record
    ```
    TODO: Use Helm chart instead? 

## Metrics Server

https://github.com/kubernetes/kops/tree/master/addons/metrics-server

For kops:
```
curl -O https://raw.githubusercontent.com/kubernetes/kops/master/addons/metrics-server/v1.8.x.yaml
```
Add this to the metrics-server container:
```
        command: ["/metrics-server"]
        args: ["--kubelet-insecure-tls"]

```
Then apply it:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/kops/master/addons/metrics-server/v1.8.x.yaml --record
```

Broken: This didn't work:
1. git clone https://github.com/kubernetes-incubator/metrics-server
1. copy metrics-server/deploy/1.8+ to the manifests directory
1. kubectl apply -R -f manifests/metrics-server/deploy/1.8+/ --record

## Elasticsearch, Logstash, and Kibana (ELK)

```
./pipeline-apply.sh theo elastic-stack stable
```

## Fluentd and Elasticsearch

```
helm fetch --untar --untardir charts stable/fluentd-elasticsearch
helm template --output-dir "manifests/${release}" --name $release charts/fluentd-elasticsearch
kubectl apply --recursive --filename manifests/fluentd-elasticsearch --record
```

## Kibana

```
helm fetch --untar --untardir charts stable/kibana
helm template --output-dir "manifests/${release}" --name $release charts/kibana
kubectl apply --recursive --filename manifests/kibana --record
```

## Keycloak

https://github.com/kubernetes/charts/tree/master/stable/keycloak
https://github.com/kubernetes/charts/tree/master/stable/postgresql
```
export release=alpha
mkdir -p "manifests/${release}"
helm fetch --untar --untardir charts stable/keycloak
helm template --values <(cat values/${release}-keycloak.values.yaml | envsubst) --output-dir "manifests/${release}" --name $release charts/keycloak
kubectl apply --recursive --filename "manifests/${release}/keycloak" --record



#helm install stable/postgresql --name keycloak-postgresql -f keycloak-postgresql.values.yaml
#helm install stable/keycloak -f <(cat keycloak.values.yaml | envsubst)
```
After Keycloak is ready, we need to setup kubelogin. Log into keycloak and do this:  
https://github.com/int128/kubelogin#1-setup-keycloak  
To create a Client Secret, you need to set the Access Type of your Keycloak Client to confidential. This will enabled a Credentials tab that has the secret.  
Then run `kops edit cluster $NAME` and add something like this:
```
spec:
  kubeAPIServer:
    oidcClientID: kubernetes
    oidcGroupsClaim: groups
    oidcIssuerURL: https://keycloak.prod.n00bernetes.com/auth/realms/master
```
Then update the cluster:
```
kops update cluster
```
Add the ClusterRoleBinding:
```
kubectl apply -f keycloak.yaml --record
```
Get your cluster's certificate. For example:
```
$ kops get secret ca
Using cluster from kubectl context: prod.n00bernetes.com

TYPE	NAME	ID
Keypair	ca	6589326552142150648609433397
Keypair	ca	keyset.yaml
$ aws s3 cp s3://$bucket_name/$NAME/pki/issued/ca/6589326552142150648609433397.crt prod.n00bernetes.com.crt
download: s3://n00bernetes-com-state-store/prod.n00bernetes.com/pki/issued/ca/6589326552142150648609433397.crt to ./prod.n00bernetes.com.crt
```
Backup your config file and create a new one. Created a variable that contains your client secret from Keycloak:
```
mv "${HOME}/.kube/config" "${HOME}/.kube/config-${NAME}"
export client_secret=899e99be-abe9-9e99-9fd9-9e462eeeb6e4
kubectl config set-credentials $NAME \
  --auth-provider oidc \
  --auth-provider-arg "idp-issuer-url=https://keycloak.${NAME}/auth/realms/master" \
  --auth-provider-arg "client-id=api.${NAME}" \
  --auth-provider-arg client-secret=YOUR_CLIENT_SECRET
```

## Drone

```
helm install stable/drone -f <(cat drone.values.yaml | envsubst)
# See http://docs.drone.io/installation/ for info on how to create OAuth app in
# Github for Drone, then run the commands that helm outputs to configure a
# valid version control provider. You'll need to replace the values for
# DRONE_ORGS, DRONE_GITHUB_CLIENT, and change the chart name from
# incubator/drone to stable/drone. You also need to set DRONE_OPEN and a
# DRONE_ADMIN for this to be useful. For example:
#
#    helm upgrade stinking-ant \
#      --reuse-values \
#      --set server.env.DRONE_OPEN="true" \
#      --set server.env.DRONE_ADMIN="evanstucker-hates-2fa" \
#      stable/drone
```

## Jenkins

```
helm install stable/jenkins -f <(cat jenkins.values.yaml | envsubst)
```

## Prometheus

```
git submodule add https://github.com/jpweber/prometheus-alert-rules.git
export serverfile_alerts=$(echo -ne 'serverFiles:\n  alerts:\n    groups:\n'; sed '/^groups:/d;s/^/      /;' prometheus-alert-rules/*.yml)
helm fetch --untar --untardir charts stable/prometheus
helm template --output-dir "manifests/${release}" --values <(cat ./values/prometheus.values.yaml | envsubst) --name $release charts/prometheus
kubectl apply --recursive --filename manifests/prometheus --record
```

## Grafana

https://blog.lwolf.org/post/going-open-source-in-monitoring-part-iii-10-most-useful-grafana-dashboards-to-monitor-kubernetes-and-services/
```
helm fetch --untar --untardir charts stable/grafana
helm template --output-dir "manifests/${release}" --values <(cat ./values/grafana.values.yaml | envsubst) --name $release charts/grafana
kubectl apply --recursive --filename manifests/grafana --record
# Password
kubectl get secrets tired-devops-grafana -o json | jq -r '.data["admin-password"]' | base64 -d; echo
```

## Bitcoind

Issues I should file:
* use bitcoin.conf, not bitcoind.conf
* Add random K8s secret instead of using default credentials.
* Add ability to make it LoadBalancer
* Make it restart when ConfigMap changes.
```
export random_password=$(date +%s | sha256sum | base64 | head -c 32)
helm install stable/bitcoind -f <(cat bitcoind.values.yaml | envsubst)
kubectl edit svc <service>
# Change Type from ClusterIP to LoadBalancer, then annotate it:
kubectl annotate service binging-cat-bitcoind 'external-dns.alpha.kubernetes.io/hostname'="bitcoind.${NAME}."
```

## SonarQube

```
helm install stable/sonarqube -f <(cat sonarqube.values.yaml | envsubst)
```

## Parity

Create a new instancegroup of maxSize/minSize 1 c4.2xlarge node for Parity (TODO: make this highly available):
```
#kops create ig parity --subnet $ZONES # This is too expensive right now
kops create ig parity --subnet us-east-1a

kops update cluster --out=. --target=terraform --yes

# At this point you will have to add the additionalPolicies from the ExternalDNS section again!
git checkout -f master data/aws_iam_role_policy_additional.nodes.$NAME_policy

terraform apply
```
Then create the Kubernetes YAML and apply it:
```
helm template --name $release --output-dir "manifests/${release}" --values <(cat ./values/parity.values.yaml | envsubst) charts/parity
kubectl apply --recursive --filename manifests/parity --record
```

## Cassandra

https://aws.amazon.com/blogs/big-data/best-practices-for-running-apache-cassandra-on-amazon-ec2/
Create a new instancegroup of 3 c4.large nodes for Cassandra:
```
kops create ig cassandra --subnet $ZONES
kops update cluster --out=. --target=terraform --yes
# At this point you will have to add the additionalPolicies from the ExternalDNS section again!
terraform apply
```
I had to download a patched-but-not-merged helm chart to get this to work:
```
cd "${HOME}/github.com/helm/charts"
git fetch origin pull/7285/head:7285
git checkout 7285
cd "${HOME}/gitlab.com/evanstucker/n00bernetes"
cp -r "${HOME}/github.com/helm/charts/incubator/cassandra" "${HOME}/gitlab.com/evanstucker/n00bernetes/charts/"
```
https://docs.datastax.com/en/cassandra/3.0/cassandra/configuration/secureConfigNativeAuth.html
I also had to hack the statefulset template. I added a command and args here:
```
spec:
  templates:
    spec:
      containers:
        - name: {{ template "cassandra.fullname" . }} 
          command: ["/bin/sh"]
          args:
          - "-c"
          - 'sed -i "s/authenticator: AllowAllAuthenticator/authenticator: PasswordAuthenticator/" /etc/cassandra/cassandra.yaml; docker-entrypoint.sh;'

```
And finally, installed it:
```
export release=alpha
mkdir -p "manifests/${release}"
helm template --values <(cat "values/${release}-cassandra.values.yaml" | envsubst) --output-dir "manifests/${release}" --name $release charts/cassandra
kubectl apply --recursive --filename "manifests/${release}/cassandra" --record
```
Then setup some new superusers and disabled the cassandra user:
```
kubectl exec -it "${release}-cassandra-0" bash
cqlsh -u cassandra -p cassandra
ALTER KEYSPACE "system_auth" WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 3};
#CREATE ROLE etucker WITH PASSWORD = 'secretpassword' AND SUPERUSER = true AND LOGIN = true;
ALTER ROLE cassandra WITH PASSWORD='REDACTED';
CREATE KEYSPACE IF NOT EXISTS ethereum WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': 3 };
```
Then expose the service by deleting the headless ClusterIP Service and creating a LoadBalancer Service:
```
sed -i '/clusterIP: None/d;s/type: ClusterIP/type: LoadBalancer/;' manifests/cassandra/templates/service.yaml
kubectl replace --force --filename manifests/cassandra/templates/service.yaml
kubectl annotate service tired-devops-cassandra 'external-dns.alpha.kubernetes.io/hostname'="cassandra.$NAME."
# This is not gitopsy. I had to manually copy the annotation to manifests/cassandra/templates/service.yaml.
```

## Redis

```
export release=alpha
mkdir -p "manifests/${release}"
helm fetch --untar --untardir charts stable/redis
helm template --output-dir "manifests/${release}" --name $release charts/redis
kubectl apply --recursive --filename "manifests/${release}/redis" --record
```

## MongoDB

```
export release=alpha
export app=blooper
mkdir -p "manifests/${release}/${app}-mongodb"
helm fetch --untar --untardir charts stable/mongodb
helm template --output-dir "manifests/${release}/${app}-mongodb" --name "${release}-${app}-mongodb" --values <(cat values/${release}-${app}-mongodb.values.yaml | envsubst) charts/mongodb
kubectl apply --recursive --filename "manifests/${release}/${app}-mongodb" --record
```

## Deploy

This is how a typical app deploy works.

First, set a release name, app, and branch:

```
export release=gary; export app=grafana; export branch=stable
```
Then copy-paste this block:
```
source custom_env.sh && \
source static_env.sh && \
helm repo update && \
helm fetch --untar --untardir "charts/${release}" "${branch}/${app}" && \
mkdir -p "manifests/${release}" && \
helm template "charts/${release}/${app}" \
  --name "${release}" \
  --output-dir "manifests/${release}" \
  --values <(cat values/${release}-${app}.values.yaml | envsubst) && \
kubectl config use-context "$NAME" && \
kubectl apply -R -f "manifests/${release}/${app}" --record
```

## OpenVPN

https://github.com/helm/charts/tree/master/stable/openvpn
To do: need to capture and use HELM_RELEASE in annotation. 
```
helm install stable/openvpn -f openvpn.values.yaml
```

## Apply all Kubernetes YAML

```
kubectl apply --recursive --filename manifests --record
```

# Deleting a cluster

Change to the cluster directory.
Source files.
terraform destroy
kops delete cluster
Delete NS records.
