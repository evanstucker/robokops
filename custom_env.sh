#!/bin/bash

# Based on https://github.com/kubernetes/kops/blob/master/docs/aws.md#prepare-local-environment

# AWS_PROFILE and AWS_REGION didn't work together when using awscli, but AWS_DEFAULT_REGION works.

# You need to edit these variables for your particular cluster.

export AWS_DEFAULT_REGION='us-west-2'
export AWS_PROFILE='n00bernetes'
export NAME='prod.n00bernetes.com'
export ZONES='us-west-2a,us-west-2b,us-west-2c'
export email='evans@n00bernetes.com'
